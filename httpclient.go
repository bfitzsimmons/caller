package caller

import (
	"net/http"
	"time"
)

// HTTPClient wraps the http client.
type HTTPClient struct {
	*http.Client
}

// InitializeHTTPClient creates a new instance of the http client.
func InitializeHTTPClient() *HTTPClient {
	return &HTTPClient{&http.Client{Timeout: time.Duration(30) * time.Second}}
}
